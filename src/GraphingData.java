import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

public class GraphingData extends JPanel {
    String color = "red";

    static double[] data1 = {};
    static double[] data2 = {};
    static double[] data3 = {};
    static double[] data4 = {};

    final int PAD = 20;

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        int w = getWidth();
        int h = getHeight();
        // Draw ordinate.
        g2.draw(new Line2D.Double(PAD, PAD, PAD, h-PAD));
        // Draw abcissa.
        g2.draw(new Line2D.Double(PAD, h-PAD, w-PAD, h-PAD));
        int length;
        double[] biggest;
        if (data1.length > data2.length){
            length = data1.length;
            biggest = data1;
        } else {
            length = data2.length;
            biggest = data2;
        } if (biggest.length < data3.length) {
            length = data3.length;
            biggest = data3;
        } if (biggest.length < data4.length) {
            length = data4.length;
            biggest = data4;
        }
        double xInc = (double)(w - 2*PAD)/(length-1);
        double scale = (double)(h - 2*PAD)/getMax(biggest);
        // Mark data1 points.
        g2.setPaint(Color.red);
        for(int i = 0; i < data1.length; i++) {
            double sx = PAD + i*xInc;
            double sy = h - PAD - scale* data1[i];
            if (i > 0){
                int j = i - 1;
                sx = PAD + i*xInc;
                sy = h - PAD - scale* data1[j];
            }
            double x = PAD + i*xInc;
            double y = h - PAD - scale* data1[i];
//            g2.fill(new Ellipse2D.Double(x - 2, y - 2, 4, 4));
            g2.draw(new Line2D.Double(sx, sy, x, y));
        }
        g2.setPaint(Color.blue);
        for(int i = 0; i < data2.length; i++) {
            double sx = PAD + i*xInc;
            double sy = h - PAD - scale* data2[i];
            if (i > 0){
                int j = i - 1;
                sx = PAD + i*xInc;
                sy = h - PAD - scale* data2[j];
            }
            double x = PAD + i*xInc;
            double y = h - PAD - scale* data2[i];
//            g2.fill(new Ellipse2D.Double(x-2, y-2, 4, 4));
            g2.draw(new Line2D.Double(sx, sy, x, y));
        }
        g2.setPaint(Color.green);
        for(int i = 0; i < data3.length; i++) {
            double sx = PAD + i*xInc;
            double sy = h - PAD - scale* data3[i];
            if (i > 0){
                int j = i - 1;
                sx = PAD + i*xInc;
                sy = h - PAD - scale* data3[j];
            }
            double x = PAD + i*xInc;
            double y = h - PAD - scale* data3[i];
//            g2.fill(new Ellipse2D.Double(x - 2, y - 2, 4, 4));
            g2.draw(new Line2D.Double(sx, sy, x, y));
        }
        g2.setPaint(Color.yellow);
        for(int i = 0; i < data4.length; i++) {
            double sx = PAD + i*xInc;
            double sy = h - PAD - scale* data4[i];
            if (i > 0){
                int j = i - 1;
                sx = PAD + i*xInc;
                sy = h - PAD - scale* data4[j];
            }
            double x = PAD + i*xInc;
            double y = h - PAD - scale* data4[i];
//            g2.fill(new Ellipse2D.Double(x - 2, y - 2, 4, 4));
            g2.draw(new Line2D.Double(sx, sy, x, y));
        }
    }

    private double getMax(double[] biggest) {
        double max = -Integer.MAX_VALUE;
        for(int i = 0; i < biggest.length; i++) {
            if(biggest[i] > max)
                max = biggest[i];
        }
        return max / 2;
    }

    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String[] toPlot = new String[1];
        toPlot[0] = "Empirical_FP_NN_Plot";
        data1 = scoreNN.main(toPlot);
        toPlot[0] = "Empirical_FP_KNN_Plot";
        data2 = scoreNN.main(toPlot);
        toPlot[0] = "Model_FP_NN_Plot";
        data3 = scoreNN.main(toPlot);
        toPlot[0] = "Model_FP_KNN_Plot";
        data4 = scoreNN.main(toPlot);
        f.add(new GraphingData());
        f.setSize(400,400);
        f.setLocation(200,200);
        f.setVisible(true);
    }
}