/**
 * Created by Martin on 11-09-2014.
 */
import java.io.*;
import java.util.*;

public class scoreNN {
    public static double[] main(String[] args) {
        /**
         * Basic stream handling
         */
        String inputFileName = null;
        if (args.length > 0 || inputFileName != null) {
            if (inputFileName == null)
                inputFileName = args[0];

            File input = new File(inputFileName);
            FileOutputStream output = null;
            try {
                output = new FileOutputStream("scoreNN_" + inputFileName, false);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            PrintStream print = System.out;
            PrintStream outputFile = new PrintStream(output);

            System.setOut(outputFile);
            double plot[] = calculateError(inputFileName);
            System.setOut(print);
            return plot;
        }
        else
        {
         System.out.println("Please specify a file name to run the results");
        }

        return new double[0];
    }
    public static double euclidianDist(double x1, double x2, double y1, double y2, double z1, double z2)
    {
        return Math.sqrt(Math.pow(x1 - x2,2) + Math.pow(y1 - y2,2) + Math.pow(z1 - z2, 2));
    }
    private static double[] calculateError(String fileName){
        double realX , realY , realZ;
        double estX , estY , estZ;

        ArrayList<Double> errorList = new ArrayList<Double>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line = null;

            while((line = reader.readLine()) != null)
            {
                if(line.substring(0, 4).equalsIgnoreCase("Real"))
                {
                   String[] split = line.split("]",2);
                   String realString = split[0].substring(7);
                   String estString = split[1].substring(7, split[1].indexOf("]"));
                   String[] realStringArr = realString.split(",");
                   realX = Double.parseDouble(realStringArr[0]);
                   realY = Double.parseDouble(realStringArr[1]);
                   realZ = Double.parseDouble(realStringArr[2]);

                   String[] estStringArr = estString.split(",");
                   estX = Double.parseDouble(estStringArr[0]);
                   estY = Double.parseDouble(estStringArr[1]);
                   estZ = Double.parseDouble(estStringArr[2]);

                   Double error = euclidianDist(realX ,estX, realY, estY, realZ, estZ);
                   errorList.add(error);

                }
            }
            Collections.sort(errorList);
            int size = errorList.size();
            double leqd = 0.0;
            double[] toPlot = new double[size + 1];
            for (int j = 0; j < size; j++){
                leqd = 0.0;

                for (int i = 0; i < size; i++){
                    if (errorList.get(i) <= errorList.get(j)){
                        leqd ++;
                    }
                }

                System.out.println("Error " + j + " is greater than or equal to " + leqd + " elements");
                leqd = (leqd / size) * 100;
//                int leq = (int)Math.round(leqd);
                if (leqd <= toPlot[j]){
                    toPlot[j + 1] = toPlot[j];
                } else {
                    toPlot[j + 1] = leqd;
                }
            }
            reader.close();
            return toPlot;
        }


        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

}
