import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * Created by philip on 9/16/14.
 */
public class Empirical_FP_KNN {
    public static void main(String[] args){
        if (args[0] == "" || args[0] == null){
            args[0] = "name";
        }
        String resultFileName = null;
        if (args.length > 0 || resultFileName != null) {
            if (resultFileName == null)
                resultFileName = args[0];

            FileOutputStream output = null;
            try {
                if (args[1] == "4"){
                    output = new FileOutputStream("ENN/Empirical_FP_NN_" + resultFileName, false);
                } else {
                output = new FileOutputStream("Empirical_FP_KNN_" + resultFileName, false);}
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            PrintStream print = System.out;
            PrintStream outputFile = new PrintStream(output);

            System.setOut(outputFile);
            NearestNeighbours neighbours = new NearestNeighbours();
            neighbours.findNeighbours(Integer.parseInt(args[1]));
        }
        else
        {
            System.out.println("Please specify a file name to run the results");
        }

    }
}
