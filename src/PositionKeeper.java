/**
 * Created by philip on 9/16/14.
 */
public class PositionKeeper {
    private GeoPosition truPos, estPos;

    public PositionKeeper(){

    }

    public PositionKeeper(GeoPosition tru, GeoPosition est){
        truPos = tru;
        estPos = est;
    }

    public GeoPosition getTruPos(){
        return truPos;
    }

    public GeoPosition getEstPos(){
        return estPos;
    }

    public void setTruPos(GeoPosition pos){
        truPos = pos;
    }

    public void setEstPos(GeoPosition pos){
        estPos = pos;
    }

}
