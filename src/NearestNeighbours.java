import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

public class NearestNeighbours {

    public static double euclidianDist(double x1, double x2, double y1, double y2, double z1, double z2)
    {
        return Math.sqrt(Math.pow(x1 - x2,2) + Math.pow(y1 - y2,2) + Math.pow(z1 - z2, 2));
    }

    public ArrayList<PositionKeeper> findNeighbours(int k) {
        ArrayList<PositionKeeper> resultList = new ArrayList<PositionKeeper>();
        String offlinePath = "MU.1.5meters.offline.trace", onlinePath = "MU.1.5meters.online.trace";
        //Construct parsers
        File offlineFile = new File(offlinePath);
        Parser offlineParser = new Parser(offlineFile);
        System.out.println("Offline File: " + offlineFile.getAbsoluteFile());

        File onlineFile = new File(onlinePath);
        Parser onlineParser = new Parser(onlineFile);
        System.out.println("Online File: " + onlineFile.getAbsoluteFile());

        ArrayList<TraceEntry> bestOffline = new ArrayList<TraceEntry>();
        //Construct trace generator
        TraceGenerator tg;
        try {
            int offlineSize = 25;
            int onlineSize = 5;
            tg = new TraceGenerator(offlineParser, onlineParser, offlineSize, onlineSize);

            //Generate traces from parsed files
            tg.generate();
            //Iterate the trace generated from the offline file
            List<TraceEntry> offlineTraces = tg.getOffline();
            List<MACAddress> averageListOff = new ArrayList<MACAddress>();
            List<TraceEntry> onlineTraces = tg.getOnline();
            List<MACAddress> averageListOn = new ArrayList<MACAddress>();
            double ss1, ss2, ss3;
            for (TraceEntry onEnt : onlineTraces) {
                PositionKeeper positionKeeper = new PositionKeeper();

                if (onEnt.getSignalStrengthSamples().getSortedAccessPoints().size() > 2) {
                    averageListOn = onEnt.getSignalStrengthSamples().getSortedAccessPoints().subList(0, 3);
                    ss1 = onEnt.getSignalStrengthSamples().getAverageSignalStrength(averageListOn.get(0));
                    ss2 = onEnt.getSignalStrengthSamples().getAverageSignalStrength(averageListOn.get(1));
                    ss3 = onEnt.getSignalStrengthSamples().getAverageSignalStrength(averageListOn.get(2));
//                    System.out.println("ss1: " + ss1 + " ss2: " + ss2 + " ss3: " + ss3);
                    for (TraceEntry offEnt : offlineTraces) {
                        if (offEnt.getSignalStrengthSamples().keySet().containsAll(averageListOn)) {
                            double m1 = offEnt.getSignalStrengthSamples().getAverageSignalStrength(averageListOn.get(0));
                            double m2 = offEnt.getSignalStrengthSamples().getAverageSignalStrength(averageListOn.get(1));
                            double m3 = offEnt.getSignalStrengthSamples().getAverageSignalStrength(averageListOn.get(2));
                            double euclSSDist = (euclidianDist(ss1, m1, ss2, m2, ss3, m3));
                            if (bestOffline.size() < k) {
                                bestOffline.add(new TraceEntry(offEnt, euclSSDist));

                            } else {
                                if (bestOffline.get(0).dist > euclSSDist) {
                                    bestOffline.set(0, new TraceEntry(offEnt, euclSSDist));

                                }
                            }
                            DistanceComparator comparator = new DistanceComparator();
                            Collections.sort(bestOffline, comparator);
                            // The entries are now sorted by best signal strength at the front of the ArrayList

                        }


                    }
                    GeoPosition realPos = new GeoPosition(onEnt.getGeoPosition().getX(), onEnt.getGeoPosition().getY());
                    // Add Z values?
                    double estimatedX = 0;
                    double estimatedY = 0;
                    int i;
                    for (i = 0; i < k; i++) {
                        estimatedX = estimatedX + bestOffline.get(i).entry.getGeoPosition().getX();
                        estimatedY = estimatedX + bestOffline.get(i).entry.getGeoPosition().getY();
                    }
                    estimatedX = estimatedX / k;
                    estimatedY = estimatedY / k;
                    GeoPosition estimatedPos = new GeoPosition(estimatedX, estimatedY);

                    positionKeeper.setTruPos(realPos);
                    positionKeeper.setEstPos(estimatedPos);
                    resultList.add(positionKeeper);
                }

                System.out.print("Real: [" + positionKeeper.getTruPos().getX() + " ," + positionKeeper.getTruPos().getY() + " ," + positionKeeper.getTruPos().getZ() + "]");
                System.out.println(" Est: [" + positionKeeper.getEstPos().getX() + " ," + positionKeeper.getEstPos().getY() + " ," + positionKeeper.getEstPos().getZ() + "]");

            }


        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        return resultList;
    }

}
